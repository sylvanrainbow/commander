IN SHORT:
  Commander executes a terminal command written in a file. 
  Default file has to be "command.txt" in the same folder, or custom path.
  Comments may be used (//, /* */). 
  Whitespace (' ', '\\t') is truncated automatically.
  Leading whitespace is ignored.
  Any double whitespace is truncated to a single space.

EXAMPLES:
  [commander]       				=>  unsafe, default local file (command.txt) used

  [commander safe]  				=>  shows computed command and asks for confirmation
  [commander info] [commander help]	=>  shows info page
  [commander file_path]         	=>  custom path to file for reading

  [commander "file path" safe]  	=>  custom path + asked for confirmation

ADDITIONAL INFO:
  You can use it for commenting out long commands, with C commenting syntax.
  Safe mode is to check the output of the program before executing the command. 
  Once you know it is safe, you can spam default.

IDEAS AND TRICKS:
  You can also make several files for different things and call them with a custom path.
  By shortening the name of the program, you could do stuff like: 
	- "commander" -> (rename) -> "c"
	- make a command file called "stuff" in the same directory
	- run [./c stuff] to execute the commented out command in "stuff" file

IN CONCLUSION:
  Basically, you can use this for bad makefiles, nicely commented compilation commands,
    and everything else where you might need a long terminal command and you wish to 
    comment it in a standardized way to know what you are doing.
