#include <stdio.h>
#include <stdlib.h>

#define PROG_NAME   "commander"                 // name of program because I haven't decided yet ;P
#define DEF_PATH    "command.txt"               // default path to the file for reading
#define MEMCHUNK    1024                        // memory reserved for command, extendable by same amount


// DECLARE GLOBAL VARS
int use_custom_path;                            // indicates whether custom file path should be used
int use_safe_mode;                              // indicates whether user confirmation required
char * command;                                 // buffer for copying file chars into
int indeX;                                      // current index in command "array"
int c, cc;                                      // intermediary variables for processing chars
int memory_reserved;                            // current memory allocated. Multiple of MEMCHUNK
FILE * fp;                                      // file pointer for opening file


// FUNCTIONS
void check_memory(void) {                       // check if more memory needs to be allocated
    if (indeX >= memory_reserved) {             // if next index larger than (reserved memory - 1)
        memory_reserved += MEMCHUNK;            // extend memory by 1 chunk
        command = realloc(command, memory_reserved * sizeof(char));         // get larger mem slot
    }
}

void add_char(int ch) {                         // add char to command buffer. Int is fgetc type
    command[indeX] = ch;                        // add char to command buffer
    indeX += 1;                                 // advance index
    check_memory();                             // check if new index fits. Fix if not
}

void process_line_comment(void) {               // ignore everything until newline or EOF
    do {
        c = fgetc(fp);                          // read chars
    } while (c != '\n' && c != EOF);            // until newline or EOF
}

void process_multiline_comment(void) {          // ignore everything until */
    c = fgetc(fp);
    while(c != EOF) {
        if (c == '*') {
            cc = fgetc(fp);                     // get next char
            if (cc != EOF) {                    // if next char is not EOF
                if (cc == '/') {                // if end sequence for multichar comments complete
                    c = fgetc(fp);              // get next char because this loop is over
                    break;                      // break out of this function
                }                               // if not end sequence */, keep looping
            }
            else {                              // if next char is EOF
                c = EOF;                        // set c to EOF so the outer loop exits also
                break;                          // nothing else to do
            }
        }
        c = fgetc(fp);
    }
}

void remove_whitespace(char * str) {            // removes whitespace from string, shortens it with \0
    int new_index = 0;                          // writing index of the new string (both are same string) 
    int old_index = 0;                          // reading index of current old string (same string both)
    int last_char_was_space = 0;                // indicator if last char read was a whitespace

    // IGNORE LEADING WHITESPACE COMPLETELY
    while (str[old_index] != '\0') {            // while not end of string
        if (   str[old_index] == ' '            // check if space
            || str[old_index] == '\t'           // or tab
        ) {
            old_index += 1;                     // just skip and forget it
        }
        else {                                  // if a legitimate char
            break;                              // escape and let the other loop process it
        }
    }

    // REMOVE MULTIPLE WHITESPACE
    while (str[old_index] != '\0') {            // while not end of string
        if (   str[old_index] == ' '            // check if space
            || str[old_index] == '\t'           // or tab
        ) {
            if (!last_char_was_space) {         // if last char was not whitespace
                last_char_was_space = 1;        // indicate that this one was
                str[new_index] = ' ';           // set space (even if tab) to new string index
                new_index += 1;                 // increment both
                old_index += 1;                 // increment both
            }
            else {
                old_index += 1;                 // spread offset if two whitespaces in a row
            }
        }
        else {                                  // if not space or tab
            str[new_index] = str[old_index];    // set old string index to new string index 
            new_index += 1;                     // increment both
            old_index += 1;                     // increment both

            last_char_was_space = 0;            // last char was not space
        }

    }

    str[new_index] = '\0';                      // cherry on top - set end of new, shortened string
}

int stringlen(char * str) {                     // returns number of chars in string ("abcd" = 4)
	int len = 0;
	while(*str != '\0') {
		str++;
		len++;
	}
	return len;
}

int str_compare(char * a, char * b) {           // returns 1 if strings are equal
    int b_length = stringlen(b);
    int a_length = stringlen(a);
    if (b_length != a_length) {
        return 0;
    }
    for (int i = 0; i < b_length; i++) {
        if (b[i] != a[i]) {
            return 0;
        }
    }
    return 1;
}

int user_pressed_y() {                          // returns 1 if user pressed [y]
    char char_buffer[] = "ab";                  // initialize array of 3 (a,b,\0) slots so
                                                //   fgets may overwrite [0][1][2] with \n

    if (    fgets(char_buffer, 3, stdin)        // get max 3-1 chars from user with fgets
            == NULL                             // check if fgets failed (returned NULL)
    ) {                                         // if fgets failed
        printf("(;^_^) Oopsie! Failed to get user input with fgets.\n");
        return 0;
    }
    else {                                      // fgets succeeded
        if (char_buffer[1] == '\n') {           // check if there was only one letter
            if (char_buffer[0] == 'y') {        // if [y]
                printf("\n");                   // add a newline to look nice 
                return 1;
            }
            else if (char_buffer[0] == 'n') {   // if [n]
                printf("You pressed [n]. Command will not execute. Exiting.\n");
                return 0;
            }
            else {                              // if some other letter
                printf("(;^_^) Oopsie! Expected input [y], but received [%c].\n", char_buffer[0]);
                return 0;
            }
        }
        else if (char_buffer[0] == '\n') {      // no letters received, just the Enter
            printf("(;^_^) Oopsie! Expected input [y], but received [].\n");
            return 0;
        }
        else {                                  // multiple letters received
            printf("(;^_^) Oopsie! Expected input [y], but received [%s...].\n", char_buffer);
            return 0;
        }
    }
}

void print_info(void) {
    printf(
        "\n"
        "  #############################################################################\n"
        " ####  __  __   __ ______ ________  #################################\n"
        " ###   \\ | \\ \\ | / \\  __/ \\      /   #######################\n"
        "###     ||  | \\||   ||__   | /\\ |     ###############\n"
        "###     ||  ||\\ |   | _/   | \\/ |     #########\n"
        " ###   /_| /_| \\_\\ /_|    /______\\   ######\n"
        " ####                               #####\n"
        "  ######################################\n"
        "\n"
        "  %s executes a terminal command written in a file. Comments may\n"
        "    be used (//, /* */). Whitespace (' ', '\\t') is truncated automatically\n"
        "\n"
        "    [%s]       =>  unsafe, default local file (%s) used\n"
        "    [%s safe]  =>  shows computed command and asks for confirmation\n"
        "    [%s info]              =>  shows this page\n"
        "    [%s help]              =>  shows this page\n"
        "    [%s file_path]         =>  custom path to file for reading\n"
        "    [%s \"file path\" safe]  =>  custom path + asked for confirmation\n"
        "\n"
        ,
        PROG_NAME, PROG_NAME, DEF_PATH, PROG_NAME, PROG_NAME, PROG_NAME, PROG_NAME, PROG_NAME
    );
}


int main (int argc, char *argv[]) {

    // INITIALIZE GLOBAL VARS
    use_custom_path = 0;                        // don't use custom path by default. Changed with args
    use_safe_mode = 0;                          // use unsafe mode by default (no confirmation)
    command = calloc(MEMCHUNK, sizeof(char));   // reserve a chunk of memory to read the command into
    memory_reserved = 1 * MEMCHUNK;             // memory reserved is the size of 1 chunk
    indeX = 0;                                  // set initial index to 0


    // PROCESS ARGUMENTS
    if (argc == 1) {                            // if just program name => default
        /* DO NOTHING */                        // everything is already set up by default
    }
    else if (argc == 2) {                       // => info || safe default || custom path
        if (   str_compare(argv[1], "info")
            || str_compare(argv[1], "help")
        ) {     
            print_info();                       // print all the info
            return 1;                           // show and exit
        }
        else if (str_compare(argv[1], "safe")) {
            use_safe_mode = 1;                  
        }
        else {                                  // if arg[1] was not safe, treat it as custom path
            use_custom_path = 1;                
        }
    }
    else if (argc == 3) {                       // => [1]custom path + [2]safe
        if (str_compare(argv[2], "safe")) {
            use_custom_path = 1;
            use_safe_mode = 1;
        }
        else {
            printf("(;^_^) Oopsie! Unrecognized first argument.\n");
            print_info();
            return 1;
        }
    }
    else {                                      // if more than 2 args
        printf("(;^_^) Oopsie! Too many arguments.\n");
        print_info();
        return 1;
    }


    // FILE HANDLING
    if (use_custom_path) {                                        
        fp = fopen (argv[1], "r");              // open file with custom path
    }
    else {
        fp = fopen (DEF_PATH, "r");             // open file with default path
    }
    
    if (fp == NULL) {                           // if opening file failed
        perror("\nERROR");                      // print error message about why file read failed
        print_info();                           // print help
        return 1;                               // non-0 means there was an error. Like exit(1)
    }
    else {                                      // if file opened successfully
        c = fgetc(fp);                          // get initial char
        while (c != EOF) {                      // if not end of file, keep reading
            if (c == '/') {                     // if suspicious character
                cc = fgetc(fp);                 // check which character follows
                if (cc != EOF) {                // if next char not EOF
                    if (cc == '/') {            // single line comment
                        process_line_comment(); // ignore all chars in current line
                                                //         and read c for next loop
                    }
                    else if (cc == '*') {               // multiline comment
                        process_multiline_comment();    // ignore chars until */ or EOF
                                                        // and read c for next loop
                    }
                    else {                      // if next char is just a char
                        add_char(c);            // add older char to command buffer
                        add_char(cc);           // add newer char to command buffer
                        c = fgetc(fp);          // get new char
                    }
                }
                else {                          // if EOF, process c and break out
                    add_char(c);                // add current char to command buffer
                    c = fgetc(fp);              // get new char
                }
            }
            else {
                if (c != '\n') {                // add new char only if not newline
                    add_char(c);                // add current char to command buffer
                }
                c = fgetc(fp);                  // get new char
            }
        }
    }

    fclose(fp);                                 // always close opened file


    // TOUCH UP OUTPUT STRING
    command[indeX] = '\0';                      // use the last index to show string is over
    remove_whitespace(command);                 // remove whitespace from string


    // HANDLE OUTPUT
    if (use_safe_mode) {                        // if safe mode
        printf(
            ">>>>>>>  SAFE MODE  <<<<<<<\n"
            "\n"
            "Would you like to execute the following [command]:\n"
            "\n"
            "  [%s]\n"
            "\n"
            "Input [y] to confirm, and [n] to exit.\n"
            , command
        );

        if (user_pressed_y()) {                 // prompts user for input and checks [y]
            system(command);                    // if [y], execute command
        }
        else {
            return 1;                           // if no [y], exit program. Warnings are posted
        }

    }
    else {                                      // if unsafe mode
        printf("\n");                           // add a newline to look nice 
        system(command);                        // execute generated clean command
    }


    // EXIT NATURALLY
    return 0;
}